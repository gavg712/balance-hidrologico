# Balance hidrológico

Esta es una presentación con hecha en Rstudio usando un proyecto Quarto. El directorio de compilación es `public`, y se puede cambiar modificando el fichero `_quarto.yml`.

Ha sido publicado en GitLab Pages usando una configuración para un sitio web estático. Se puede modificar la configuración en el fichero `.gitlab-ci.yml`.

[Ver presentación](https://gavg712.gitlab.io/balance-hidrologico/)

## Licencia

GPL-3
