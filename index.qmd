---
title: "El Balance hidrometeorológico"
subtitle: "Un introducción a escala de cuenca"
author: Gabriel Gaona
date: 2022-06-15
format:
  revealjs:
    theme: tema_ikiam.scss
    logo: img/logo.svg
    footer: "Balance hidrometeorlógico de una cuenca | Gabriel V. Gaona"
    preview-links: true
    chalkboard: true
    #multiplex: true
    fragments: true
    show-slide-number: all
    scrollable: true
toc: true
lang: es-Ec
bibliography: references.bib
---

# Introducción

::: notes
-   Esta clase se da en el marco de la oferta de trabajo número
:::

## Sobre mi!

::: columns
::: {.column width="70%"}
::: callout-tip
## Soy Gabriel Gaona.

-   Ingeniero ambiental y máster SIG
-   Análisis de datos espaciales, software SIG y programación.
-   Interés en investigación sobre \[eco\]hidrología, meteorología y ecología del paisaje.
-   Más de 10 años de experiencia profesional.
-   Activista de datos y ciencia abierta, y activista de software libre.
:::
:::

::: {.column width="30%"}
![](https://gavg712.gitlab.io/resume/img/gaona_gabriel.png){style="margin:15px; box-shadow: 0px 0px 20px steelblue;border-radius: 10px"}

::: {.p style="font-size: 1.2rem; line-height: normal; text-align: center;"}
Ver más detalles en: [Resume](https://gavg712.gitlab.io/resume/index.es.html)
:::
:::
:::

## Sobre la charla

::: callout-note
## Para considerar

Esta será una clase demostrativa con la temática del *"Balance Hidrometeorológico"*. Los contenidos de la charla se basan en diferentes libros y artículos científicos actuales.
:::

::: callout-tip
## Objetivo de la clase

Al final de esta clase se espera que el estudiante comprenda la complejidad del sistema eco-hidrológico de una cuenca hidrográfica, desde la perspectiva del balance hidrológico.
:::

::: notes
-   La presentación se ha diseñado para proteger su visión, considerando que ustedes van a pasar un tiempo con la mirada fija en la pantalla.
-   Por favor si alguien tiene problemas para visualizar ya sea por visión daltónica, o cansancio visual, me lo puede indicar de modo que yo pueda intentar cambiar algo.
:::

## Conceptos básicos

::: panel-tabset
### Ciclo hidrológico

::: columns
::: {.column width="60%"}
![](img/water_cicle.png){fig-align="center"} [Imagen tomada de @singh2021]{style="font-size:1.2rem"}
:::

::: {.column width="40%"}
::: callout-tip
## Definición

El ciclo hidrológico, también conocido como el *ciclo del agua*, es sistema de reciclado normal del agua en la tierra [@singh2021] .
:::
:::
:::

### Cuenca hidrográfica

::: columns
::: {.column width="60%"}
![](https://amazonwaters.org/wp-content/uploads/2016/05/bl2basin-1.jpg){fig-align="center"}

::: {.p style="font-size: 1.2rem; line-height: normal;"}
Izq) Mapa de cuencas hidrográficas [@singh2021]; Der) Fotografía de afluente del Río Napo [@zapata-ríos]
:::
:::

::: {.column width="40%"}
::: callout-tip
## Definición

Una cuenca hidrográfica es la delimitación topográfica del flujo del agua.
:::

![](https://amazonwaters.org/wp-content/uploads/2015/10/RioHollin_.jpg)
:::
:::

### Escala

::: columns
::: {.column width="70%"}
![](img/modelling_scales.svg){fig-align="center"}
:::

::: {.column width="30%"}
::: {.p style="font-size: 1.2rem; line-height: normal; position: absolute; bottom: 10px;"}
"Human" scale shown in the context of "hydrologic" scale and of the approximate space and time ranges corresponding to the main levels of scale used for conceptual representations of physical processes. [@klemes1983]
:::
:::
:::
:::

::: notes
-   El ciclo del agua ocurre en todos los niveles y escalas
-   En cada escala el ciclo del agua puede volverse más complejo o sus componentes básicos pueden ser diferentes.
-   El límite de la cuenca es la divisoria de aguas. Todo lo que caiga dentro del polígono tiende a escurrir hacia un mismo punto de salida.
-   Una cuenca puede estar formada por subcuencas
-   Se pueden clasificar en distintos tipos de acuerdo a su punto de evacuación: Endorreicas y Exorreicas
-   Según Klemes, Nuestros estudios hidrológicos, se producen únicamente a escalas en las que el humano puede entender el cliclo hidrológico.
:::

# El Balance del agua en una Cuenca Hidrográfica

::: callout-tip
## Definamos el Balance Hidrometeorológico

Considerando el ciclo hidrológico y la escala de cuenca, el BH es el equilibrio entre todos los recursos hídricos que ingresan a la cuenca y los que salen de la misma, en un tiempo determinado.
:::

::: notes
- El BH se puede comparar a un ejercicio básico de contabilidad, donde de ingreso y salida de recursos.
- El BH se establece para un lugar y tiempo determinados.

:::

## Ecuación fundamental {.scrollable}

::: columns
::: {.column width="20%"}
![](img/process_esquema_0.1.svg)
:::

::: {.column width="80%"}
$$
\int {I} \delta t= \int {S} \delta t + \int {O} \delta t
$$

Donde, $\int {I} \delta t$ son todas las **entradas** de agua; $\int {S} \delta t$ es el agua **retenida** en el sistema; $\int {O} \delta t$ son todas las **salidas** de agua

$$
\Delta I = \Delta S + \Delta O
$$

$\Delta x$ se consideran como valores discretos en el tiempo del ciclo del agua
:::
:::

::: {.notes}
- La ecuación también se conoce como _"Ecuación de conservación de la masa"_
- Debido a que no podemos medir todos los estados de las entradas y salidas en el tiempo y el espacio, la forma como logramos hacer un balance es delimitando el tiempo y el espacio.
- Para reflexionar: El diagrama de flujo, da a entender que se trata de un sistema lineal. Pero *¿Es un sistema lineal?*
:::

## Simplificando el sistema

::: columns
::: {.column width="60%"}
$$
P =  S  + Q
$$

Donde: $P$ es la precipitación, $S$ es el almacenamiento y $Q$ el caudal
:::

::: {.column width="40%"}
![](img/water_cycle_0.svg)
:::
:::

::: callout-note
## Nótese que ...

Intencionalmente se han eliminado todas las interacciones de este sistema para simplificar el balance hidrológico.
:::

::: {.notes}
- Entendamos entonces que implica la simplificación de la ecuación fundamental de la hidrología. 
- Para eso debemos transformar las variables de la ecuación en variables del ciclo hidrológico.
- Para reflexionar: *Entonces ¿Nuestro modelo representa la realidad?*
:::

## Un sistema con entradas y salidas de agua

::: columns
::: {.column width="50%"}
![](img/water_cycle_1.svg)
:::

::: {.column width="50%"}
Descomponiendo la ecuación inicial tenemos:

$$
\color{#29a0dc}{P} = S + \color{#b27147}{Q} \\
\Downarrow \\
$$
:::
:::

$$
\color{#29a0dc}{P_l + P_n + P_i} =  S  + \color{#b27147}{ET + Q_c + Q_f}
$$

::: notes
-   Al la imagen le agregamos un motor (el sol) y algunas interacciones. Ahora ya se ve como un ciclo. Y también la ecuación se puede descomponer.
-   Sigan los colores por favor.
-   He descompuesto la precipitación en: Lluvia, Nieve e Intercepción, pero pueden haber más.
-   De igual manera he descompuesto las salidas como: Evapotranspiración, Escorrentía y Otras Pérdidas
-   En este punto quiero destacar que el BH dependerá de nuestra capacidad para medir o calcular las variables que intervienen.
:::

------------------------------------------------------------------------

::: columns
::: {.column width="40%"}
![](img/process_esquema_1.1.svg)
:::

::: {.column width="60%"}
![](img/water_cycle_1.svg)
:::
:::

$$
\color{#29a0dc}{P_l + P_n + P_i} =  S  + \color{#b27147}{ET + Q_c + Q_f}
$$

::: notes
-   Aquí vemos como el diagrama de flujo también cambia.
-   Todavía se ve como si hubieran interrelaciones simples.
-   No se ha descompuesto los procesos durante el almacenamiento en la cuenca.
-   Pregunta para reflexionar: **¿Cómo medimos el almacenamiento en la cuenca?**
:::

## BH de un sistema complejo

::: columns
::: {.column width="55%"}
![](img/runoff_generation.png){fig-align="center"}

::: {.p style="font-size: 1.2rem; line-height: normal;"}
Figure. Physical processes involved in gunoff generation  [@celleri2018]
::: 
:::

::: {.column width="45%"}
$$
\color{#29a0dc}{P_l + P_n + P_i} = \\  \color{#64a462}{S_{emb} + S_{veg} + S_{gw} + S_s} \\ \color{#b27147}{+ ET + Q_c + Q_f}
$$

:::
:::

![](img/process_esquema_2.1.svg){fig-align="center"}

::: notes
-   En la imagen podemos ver más elementos que en nuestros ejemplos anteriores.
-   No ha cambiado nada en el concepto del BH. Existen entradas, salidas y los procesos
-   Solo se ve más complejo porque el sistema natural es complejo.
-   Los nuevos componentes implican nuevos formas de medición, más posibilidad de error en el cálculo del BH.
-   Pero también, una representación más real del sistema natural en nuestro modelo.
:::

# El porqué de un BH

Un BH permite conocer la dinámica de una cuenca hidrográfica y el uso del agua de sus actores.

-   **Applicable**. Una creciente colección de investigaciones y estudios de casos aplican el balance hidrológico a diversos recursos, desde el caudal de los arroyos y manantiales hasta las respuestas de las plantas y los animales [@tercek2021; @xu1998]

-   **Accesible**. Un modelo de balance hidrológico traduce la temperatura y precipitaciones en medidas de disponibilidad de agua más relevantes desde el punto de vista ecológico.

-   **Estandarizado**. Ha sido ampliamente aceptado como herramienta de modelamiento y evaluación. Muchos organismos internacionales lo emplean.

::: notes
-   En comparación con la temperatura y las precipitaciones, estas variables que genera un BH suelen predecir mejor la respuesta de las plantas y los animales, o de otros fenómenos naturales.
:::

# Referencias
::: {#refs}
:::

# Gracias por su atención!

<hr>
Gabriel V. Gaona
<br>Aplicante
<br>E-mail: gavg712@gmail.com
<br>Teléfono: +593 991 665 888
