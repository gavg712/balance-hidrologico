graph TD
    
    subgraph Entradas
    Lluvi[Lluvia]
    Nieve
    Inter[Intercepción]
    end
    Lluvi --> B[(Procesos)]
    Nieve --> B
    Inter --> B
    B -->|Escorrentía| Caudal
    B -->|Escorrentía| fuga
    subgraph Salidas
    Caudal
    fuga[Otras cuencas]
    end

    style Entradas fill:#d5e4fdaa,stroke-width:1px,color:blue
    style Salidas fill:#ad7fa8aa,stroke-width:1px,color:blue
    style fuga fill:#bbbbbbaa,stroke-width:2px,color:gray,stroke:#555
