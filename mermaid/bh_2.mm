graph TD
    
    subgraph Entradas
    Lluvi[Lluvia]
    Inter[Intercepción]
    Nieve
    end
    Lluvi --> infil
    Lluvi --> perco
    Lluvi --> stemf
    Lluvi --> embal
    Nieve --> |Sublimación| ET
    Nieve --> infil
    Inter --> stemf
    subgraph Procesos
    stemf[Stem flow]
    infil[Infiltración]
    perco[Percolación]
    rootz[Captura Raíces]
    embal[Embalse]
    infil --> rootz
    infil --> perco
    end
    Lluvi -->|Escorrentía| Caudal
    stemf --> ET
    stemf --> Caudal
    rootz -->|ET/ETP| ET
    embal -->|Exportación| fuga
    perco -->|Drenajes| fuga
    perco -->|Drenajes| Caudal
    subgraph Salidas
    Caudal
    ET
    fuga[Otras cuencas]
    end

    style Entradas fill:#d5e4fdaa,stroke-width:1px,color:blue
    style Salidas fill:#ad7fa8aa,stroke-width:1px,color:blue
    style fuga fill:#bbbbbbaa,stroke-width:2px,color:gray,stroke:#555