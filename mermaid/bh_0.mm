graph TD
    
    subgraph Entradas
    Lluvi[Todas la entradas]
    end
    Lluvi --> B[(Procesos)]
    B --> Caudal
    subgraph Salidas
    Caudal[Todas las salidas]
    end

    style Entradas fill:#d5e4fdaa,stroke-width:1px,color:blue
    style Salidas fill:#ad7fa8aa,stroke-width:1px,color:blue
